# POOP Menu Driven

POOP - Python Object-Oriented Program

This project contains a template for menu-driven application respecting object-oriented programming paradigm. Example is in Python programming language 

## Description

This project contains:

- Object-oriented main program
- Menu defined in object-oriented way

Menu consists of two main aspects:

- Options
  - Option1: prints hello world
  - Option2: Prompts user name and says hello to the user
- Run -> starts menu-driven behaviour

## Example program run

```
Program starting.
Options:
1 - Hello world
2 - Greeting
0 - Exit
Your choice: 1
Hello world!

Options:
1 - Hello world
2 - Greeting
0 - Exit
Your choice: 2
Insert name: John
Hello John!

Options:
1 - Hello world
2 - Greeting
0 - Exit
Your choice: 0

Exiting program.
Program starting.
```