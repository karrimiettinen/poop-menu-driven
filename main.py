from menu import Menu

class Main:
    def __init__(self) -> None:
        print("Program starting.")
        # 1. Initialize
        _menu = Menu()
        # 2. Operate
        _menu.run()
        # 3. Cleanup
        #print("Program ending.")
        return None
    
if __name__ == "__main__":
    app = Main()
