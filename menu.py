class Menu:
    def hello_world(self) -> None:
        print("Hello world!")
        return None
    def greeting(self) -> None:
        name = input("Insert name: ")
        print(f"Hello {name}!")
        return None
    def askChoice(self) -> int:
        choice = -1
        feed = input("Your choice: ")
        if feed.isdigit():
            choice = int(feed)
        return choice
    def run(self) -> None:
        choice = -1
        while choice != 0:
            print("Options:")
            print("1 - Hello world")
            print("2 - Greeting")
            print("0 - Exit")
            choice = self.askChoice()
            if choice == 1:
                self.hello_world()
            elif choice == 2:
                self.greeting()
            elif choice == 0:
                pass
            else:
                print("Unknown option, try again.")
            print("")
        print("Exiting program.")
        return None
